<?php

use App\FileBrowser\FileBrowser;

class FileBrowserTest extends PHPUnit_Framework_TestCase
{

    const ROOT_DIRECTORY = __DIR__ . '/../root';

    /**
     * @var FileBrowser
     */
    protected $fileBrowser;

    public function setUp()
    {
        $this->fileBrowser = new FileBrowser(self::ROOT_DIRECTORY);
    }

    public function testInit()
    {
        $fileBrowser = new FileBrowser('');

        $this->assertEquals(FileBrowser::class, get_class($fileBrowser));
    }

    /**
     * @dataProvider pathDataSets
     */
    public function testGetFilesInCurrentPath($root, $currentPath, $filename, $nestedFolderName, $upOneLevel)
    {
        $this->fileBrowser->setRootPath($root);
        $this->fileBrowser->setCurrentPath($currentPath);

        $filesArray = $this->fileBrowser->getFilesInCurrentPath();

        $this->assertTrue(is_array($filesArray));
        $this->assertNotFalse(array_search($filename . '.json', $filesArray['files']));
        $this->assertNotFalse(array_search($filename . '.txt', $filesArray['files']));

        if ($nestedFolderName) {
            $this->assertNotFalse(array_search($nestedFolderName, $filesArray['folders']));
        } else {
            $this->assertFalse(array_search($nestedFolderName, $filesArray['folders']));
        }

        if ($upOneLevel) {
            $this->assertNotFalse(array_search('..', $filesArray['folders']));
        } else {
            $this->assertFalse(array_search('..', $filesArray['folders']));
        }
    }

    public function testGetFilesInCurrentPathWithExtensionFilter()
    {
        $extension = 'txt';
        $this->fileBrowser->setExtensionFilter([$extension]);

        $filesArray = $this->fileBrowser->getFilesInCurrentPath();

        $this->assertTrue(is_array($filesArray));
        $this->assertEquals(1, count($filesArray['files']));
        $this->assertNotFalse(array_search('root.' . $extension, $filesArray['files']));
    }

    public function pathDataSets()
    {
        return [
            [
                self::ROOT_DIRECTORY,
                '',
                'root',
                'level_1',
                false,
            ],
            [
                self::ROOT_DIRECTORY,
                'level_1',
                'level_1',
                'level_2',
                true,
            ],
            [
                self::ROOT_DIRECTORY,
                'level_1/level_2',
                'level_2',
                'level_3',
                true,
            ],
            [
                self::ROOT_DIRECTORY,
                'level_1/level_2/level_3',
                'level_3',
                false,
                true,
            ],
        ];
    }

    public function testGetFilesInCurrentPath_FilesAndFoldersAreOrdered()
    {
        $this->fileBrowser->setCurrentPath('level_1');
        $filesArray = $this->fileBrowser->getFilesInCurrentPath();

        $this->assertEquals('level_1.json', $filesArray['files'][0]);
        $this->assertEquals('level_1.txt', $filesArray['files'][1]);

        $this->assertEquals('..', $filesArray['folders'][0]);
        $this->assertEquals('level_2', $filesArray['folders'][1]);
    }

    /**
     * @dataProvider fileQueryArrayDataSets
     */
    public function testGetFileQueryArray($currentPath, $file, $expectedArray)
    {
        $this->fileBrowser->setCurrentPath($currentPath);

        $this->assertEquals($expectedArray, $this->fileBrowser->getFileQueryArray($file));
    }

    public function fileQueryArrayDataSets()
    {
        return [
            [
                'level_1',
                '..',
                [],
            ],
            [
                'level_1/level_2',
                '..',
                [
                    'current_path' => 'level_1',
                ],
            ],
            [
                '',
                'level_1',
                [
                    'current_path' => 'level_1',
                ],
            ],
            [
                'level_1',
                'level_2',
                [
                    'current_path' => 'level_1/level_2',
                ],
            ],
        ];
    }

}
