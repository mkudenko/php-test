<?php namespace App\FileBrowser;

use App\Contracts\FileBrowserInterface;

class FileBrowser implements FileBrowserInterface
{

    /**
     * @var string
     */
    protected $rootPath;

    /**
     * @var string
     */
    protected $currentPath;

    /**
     * @var array
     */
    protected $extensionFilter;

    /**
     * Construct
     *
     * @param string $rootPath Directory path to list files
     * @param string $currentPath Current directory path to list files - will default to $rootPath if null
     * @param array $extensionFilter Array of file extensions to filter - will not apply a filter if empty
     */
    function __construct($rootPath, $currentPath = null, array $extensionFilter = array())
    {
        $this->rootPath = $rootPath;
        $this->currentPath = $currentPath;
        $this->extensionFilter = $extensionFilter;
    }

    /**
     * Set private root path
     */
    function setRootPath($rootPath)
    {
        $this->rootPath = $rootPath;
    }

    /**
     * Set private current path
     */
    function setCurrentPath($currentPath)
    {
        $this->currentPath = $currentPath;
    }

    /**
     * Get private current path
     */
    function getCurrentPath()
    {
        return $this->currentPath;
    }

    /**
     * Set private extension filter
     */
    function setExtensionFilter(array $extensionFilter)
    {
        $this->extensionFilter = $extensionFilter;
    }

    /**
     * Get files using currently-defined object properties
     * @return array Array of files within the current directory
     */
    function getFilesInCurrentPath()
    {
        $files = [
            'files' => [],
            'folders' => [],
        ];

        $dirPath = $this->rootPath . '/' . $this->currentPath;

        $dir_handler = opendir($dirPath);

        if (!$dir_handler) {
            throw new \Exception();
        }

        while (($file = readdir($dir_handler)) !== false) {
            if ($file == '.') {
                continue;
            }
            if ($file == '..') {
                if ($this->currentPath != '') {
                    $files['folders'][] = $file;
                }
                continue;
            }

            if (is_dir($dirPath . '/' . $file)) {
                $files['folders'][] = $file;
            } elseif ($this->filePassesFilter($file)) {
                $files['files'][] = $file;
            }
        }

        sort($files['files']);
        sort($files['folders']);

        return $files;
    }

    public function getFileQueryArray($file)
    {
        $pathArray = ($this->currentPath == '') ? [] : explode('/', $this->currentPath);


        if ($file == '..') {
            $key = array_search(end($pathArray), $pathArray);
            unset($pathArray[$key]);
        } else {
            $pathArray[] = $file;
        }

        $return = [];

        if (!empty($pathArray)) {
            $return['current_path'] = implode('/', $pathArray);
        }

        return $return;
    }

    protected function filePassesFilter($file)
    {
        if (empty($this->extensionFilter)) {
            return true;
        }

        $fileExtension = end(explode('.', $file));

        return in_array($fileExtension, $this->extensionFilter);
    }

}
