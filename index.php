<?php
require_once('vendor/autoload.php');

use App\FileBrowser\FileBrowser;

$fileBrowser = new FileBrowser('root');

$currentPath = (isset($_GET['current_path'])) ? $_GET['current_path'] : '';
$extensionFilter = (isset($_GET['filter'])) ? $_GET['filter'] : '';

if ($currentPath) {
    $fileBrowser->setCurrentPath($currentPath);
}

if ($extensionFilter) {
    $fileBrowser->setExtensionFilter($extensionFilter);
}

$files = $fileBrowser->getFilesInCurrentPath();


?>

<!doctype html>
<html lang="en">
<head>
    <title>File browser</title>
</head>
<body>

    <?php foreach ($files['folders'] as $folder): ?>
        <div><strong><a href="?<?php print http_build_query($fileBrowser->getFileQueryArray($folder)); ?>">
            <?php print $folder; ?>
        </a></strong></div>
    <?php endforeach; ?>


    <?php foreach ($files['files'] as $file): ?>
        <div><?php print $file; ?></div>
    <?php endforeach; ?>

</body>
</html>
